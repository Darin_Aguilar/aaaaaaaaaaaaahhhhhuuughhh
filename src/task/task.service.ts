import { Injectable } from '@nestjs/common';
import { Task, TaskStatus } from './task.entity';
import { v4 } from 'uuid';
import { UpdateTaskDTO } from './dto/task.dto';

@Injectable()
export class TaskService {
  private task: Task[] = [
    {
      id: '1',
      title: 'task 1',
      description: 'some task',
      status: TaskStatus.PENDING,
    },
  ];

  getAllTask() {
    return this.task;
  }
  createTask(title: string, description: string) {
    const task = {
      id: v4(),
      title,
      description,
      status: TaskStatus.PENDING,
    };
    this.task.push(task);

    return task;
  }
  deleteTask(id: string) {
    this.task = this.task.filter((task) => task.id !== id);
  }
  getTaskById(id: string): Task {
    return this.task.find((task) => task.id === id);
  }
  updateTask(id: string, updateFields: UpdateTaskDTO): Task {
    const task = this.getTaskById(id);
    const newTask = Object.assign(task, updateFields);
    this.task = this.task.map((task) => (task.id === id ? newTask : task));
    return newTask;
  }
}
