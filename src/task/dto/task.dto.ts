import { Transform } from 'class-transformer';
import { TaskStatus } from '../task.entity';
import {
  IsIn,
  IsNotEmpty,
  IsOptional,
  IsString,
  MinLength,
} from 'class-validator';

export class CreateTaskDTO {
  @IsString()
  @IsNotEmpty()
  @MinLength(3)
  title: string;
  @IsString()
  description: string;
}

export class UpdateTaskDTO {
  @IsString()
  @IsOptional()
  @MinLength(3)
  title?: string;
  @IsString()
  @IsOptional()
  description?: string;
  @IsString()
  @IsOptional()
  @IsIn([TaskStatus.IN_PROGRES, TaskStatus.PENDING, TaskStatus.DONE])
  @Transform(({ value }) => value.trim().toUpperCase())
  status?: TaskStatus;
}
